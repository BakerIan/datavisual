/**
 * Main application file
 */

'use strict';

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var express = require('express');
var config = require('./config/environment');


// Setup server
var app = express();
var server = require('http').createServer(app);
var mongoose = require('mongoose');




require('./config/express')(app);

//setup socket.io server and attach it to the express server
var io = require('socket.io')(server);

//launch the twitter stream connexion + attach it to the socket io listeners
var socketManager = require('./logic/sockets').launch(io);




//setup routes (ad the /api/state from the socket manager as well)
var errors = require('./components/errors');
// Returns the state of the socket and twitter connexions
app.get('/api/state', function(req,res){
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify(socketManager.getState()));
});

// All undefined asset or api routes should return a 404
//app.route('/:url(api|auth|components|app|bower_components|assets)/*')
// .get(errors[404]);




// All other routes should redirect to the index.html
app.route('/*')
  .get(function(req, res) {
    res.sendfile(app.get('appPath') + '/index.html');
});


// Start server
server.listen(config.port, config.ip, function () {
  console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
});

// Expose app
exports = module.exports = app;
