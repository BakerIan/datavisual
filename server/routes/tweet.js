
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), //mongo connection
    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'); //used to manipulate POST

var Tweet = require('../model/tweet');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(methodOverride(function(req, res){
        if (req.body && typeof req.body === 'object' && '_method' in req.body) {
          // look in urlencoded POST bodies and delete it
          var method = req.body._method;
          delete req.body._method;
          return method;
        }
  }));

  router.param('id', function(req, res, next, id) {
    console.log('validating' + id + ' exists');
    //find the ID in the database
    mongoose.model('tweet').findById(id, function(err, tweet){
      //if it isn't found we are going to respond with 404
      if (err) {
        console.log(id + 'was not found');
        res.status(404);
        var err = new Error('Not Found');
        err.status = 404;
        res.format({
          hmtl: function() {
            next(err);
          },
          json: function(){
            res.json({message : err.status + ' ' + err});
          }
        });

      } else {
        //uncomment this next line if you want to see every JSON document response for every GET/PUT/DELETE call
        //console.log(tweet);
        //once validation is done save the new item in the req
        req.id = id;
        // go to the next thing
        next();
      }
    });
  });

//Get the individual text by Mongo ID
router.get('/tweets/:id/edit', function(err, tweet){
  //search for the tweet within mongoose
  mongoose.model('tweet').findById(req.id, function(err, tweet){
    if (err) {
      console.log('GET Error: There was a problem retrieving: ' + err);
    } else {
      //Return the tweet
      console.log('GET Retrieving ID: ' + tweet._id);

      res.format({
        //HTML response will render the 'edit.jade' template
        htm: function(){
          res.render('tweets/edit', {
            tweet_id: tweet.tweet_id,
            text : tweet.text,
            channels : tweet.channels,
            keywords : tweet.keywords
          });
        },
        json: function(){
          res.json(tweet);
        }
      });
    }
  });
});

router.delete('tweets/:id/edit', function(req, res){
  //find tweet by ID
  mongoose.model('tweet').findById(req.id, function(err, tweet){
    if (err) {
      return console.error(err);
    } else {
      //Returning success messages saying it was deleted
      console.log('DELETE remvoing ID: ' + tweet._id);
      res.format({
        //HTML returns us back to the main page
        html: function(){
          res.redirect('/tweets');
        }
      })
    }
  });
});

router.route('/')
  //GET all tweets
  .get(function(req, res, next) {
    //retrieve all tweets from Mongo
    mongoose.model('tweet').find({}, function(err, tweets) {
      if (err) {
        return console.error(err);
      } else {
        //respond to both HTML and JSON. JSON responses require 'Accept: application/json;' in the Request Header
        res.format({
            //HTML response will render the index.jade file in the views/blobs folder. We are also setting "blobs" to be an accessible variable in our jade view
          html: function(){
           res.render('tweets/index', {
              title: 'All Tweets',
              "tweets" : tweets
          });
        },
          //JSON response will show all tweets in JSON format
          json: function(){
            res.json(tweets);
          }
        });
      }
    });
  })
  //POST a new tweet
  .post(function(req, res) {
    // Get values from POST request. These can be done through forms or REST calls. These rely on the "name" attributes for forms
    var tweet_id= req.body._id;
    var text= req.body.text;
    var channels = req.body.channels;
    var keywords = req.body.keywords;
    //call the create function for our database
    mongoose.model('tweet').create({
      tweet_id: tweet_id,
      text: text,
      channels: channels,
      keywords : keywords
    }, function(err, tweet) {
      if (err){
        res.send("There was a problem adding the tweet to the db");
      } else {
        //Tweet has been created
        console.log('POST creating new tweet: ' + tweet);
        //res.format({
            //HTML response will set the location and redirect back to the home page. You could also create a 'success' page if that's your thing
          //html: function(){
            //If it worked, set the header so the address bar doesn't still say /adduser
            //res.location('tweets');
            // And forward to success page
            //res.redirect('/tweets');
        //  },
          //JSON response will show the newly created tweet
          //json: function(){
          //  res.json(tweet);
        //  }
        //});
      }
    })
  });

router.put('tweets/:id/edit', function(req, res){
    //Get our REST or form values.
    var tweet_id = req.body._id;
    var text = req.body.text;
    var channels = req.body.channels;
    var keywords = req.body.keywords;

    //find the document by ID
    mongoose.model('Tweet').findById(req.id, function(err, tweet){
      //update it
      tweet.update({
        tweet_id: tweet_id,
        text : text,
        channels : channels,
        keywords : keywords
      }, function (err, tweetID){
        if (err) {
          res.send("There was a problem updating the information to the database: " + err);
        } else {
          //HTML responds by going back to the page
          res.format({
            html: function(){
              res.redirect("/tweets/" + tweet._id)
            },
            json: function(){
              res.json(tweet);
            }
          });
        }
      })
    });
});

//DELETE a tweet by ID
router.delete('tweets/:id/edit', function(req, res) {
  //find tweet by id
  mongoose.model('Tweet').findbyId(req.id, function(err, tweet) {
    if (err) {
      return console.error(err);
    } else {
      //remove it from mongo
      tweet.remove(function(err, tweet){
      console.log('DELETE removing ID: ' + tweet._id);

        res.format({
          //HTML returns us back to the main page
          html: function(){
            res.redirect('/tweets');
          },
          //JSON returns the item with the message that it has been deleted
          json: function(){
            res.json({message : 'deleted',
              item : tweet

            });
          }
        });
      });
    }
  });
});


module.exports = router;
