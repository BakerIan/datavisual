var mongoose = require('mongoose');
var tweetSchema = new mongoose.Schema({
  tweet_id: String,
  user_id: String,
  text : String,
  channels : [String],
  keywords : [String],
  _id: String,
  type: String,
  in_reply_to_tweet_id: Number,
  in_reply_to_user_id: Number,
  created_at: Date,
  lang: String,
  coordinates: [String]
});

var tweet = mongoose.model('tweet', tweetSchema);
module.exports = tweet;
