var mongoose = require('mongoose');
var querySchema = new mongoose.Schema({
  keyword: String,
  num_of_tweets: Number,
  created_at: Date,
  end: Date,
  duration: Number
});

var query = mongoose.model('query', querySchema);
module.exports = query;
